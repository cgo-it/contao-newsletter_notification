<?php 

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten Götzinger (info@cgo-it.de)
 * @package    newsletter_notification
 * @license    GNU/LGPL
 * @filesource
 */


/**
 * Miscellaneous
 */
$GLOBALS['TL_LANG']['tl_newsletter_channel']['nl_notification_legend'] =  'Benachrichtigungen bei Eintragen/Austragen von Abonnenten';
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_active'] =  array( 'Aktivieren' , 'Aktiviert die Benachrichtigungen' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_subscribe'] =  array( 'Beim Eintragen' , 'Versendet eine Benachrichtigung, wenn sich ein neuer Abonnent einträgt.' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_subscribe_text'] =  array( 'Abonnementbenachrichtigung' , 'Text, der an die Empfänger geschickt werden soll, wenn sich ein Benutzer für einen Newsletter anmeldet.' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_unsubscribe'] =  array( 'Beim Austragen' , 'Versendet eine Benachrichtigung, wenn sich ein Abonnent austrägt.' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_unsubscribe_text'] =  array( 'Kündigungsbenachrichtigung' , 'Text, der an die Empfänger geschickt werden soll, wenn sich ein Benutzer von einem Newsletter abmeldet.' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_recipients'] =  array( 'Empfänger' , 'Empfänger der Benachrichtigung (mehrere E-Mail-Adressen mit ";" trennen)' );
$GLOBALS['TL_LANG']['tl_newsletter_channel']['text_subscribe'] = 'Der User mit der Email-Adresse ##email## hat sich in den Newsletter "##channel##" eingetragen.';
$GLOBALS['TL_LANG']['tl_newsletter_channel']['text_unsubscribe'] = 'Der User mit der Email-Adresse ##email## hat sich aus dem Newsletter "##channel##" ausgetragen.';
