<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten G�tzinger (info@cgo-it.de)
 * @package    newsletter_notification
 * @license    GNU/LGPL
 * @filesource
 */


/**
 * Run in a custom namespace, so the class can be replaced
 */
namespace cgoIT\newsletter_notification;


/**
 * Class NewsletterNotification
 */
class NewsletterNotification extends \Backend {

	public function activateRecipient($strEmail, $arrRecipients, $arrChannels) {
		$this->import('Database');
		
		$arrNewsletterChannels = $this->Database->prepare("SELECT * FROM tl_newsletter_channel WHERE id in (".implode(',', array_map('intval', $arrChannels)).")")
									->execute()
									->fetchAllAssoc();
		foreach ($arrNewsletterChannels as $arrNewsletterChannel) {
			if (intval($arrNewsletterChannel['notification_active']) === 1 && intval($arrNewsletterChannel['notification_on_subscribe']) === 1) {
				$email = new \Email();
				$email->subject = 'Anmeldung zum Newsletter "'.$arrNewsletterChannel['title'].'"';
				
				$strText = str_replace('##channel##', $arrNewsletterChannel['title'], $arrNewsletterChannel['notification_on_subscribe_text']);
				$strText = str_replace('##email##', $strEmail, $strText);
				$strText = str_replace('##domain##', \Idna::decode(\Environment::get('host')), $strText);
				
				$email->text = $strText;
				$email->sendTo($arrNewsletterChannel['notification_recipients']);
			}
		}
	}

	public function removeRecipient($strEmail, $arrChannels) {
		$this->import('Database');
		
		$arrNewsletterChannels = $this->Database->prepare("SELECT * FROM tl_newsletter_channel WHERE id IN (".implode(',', array_map('intval', $arrChannels)).")")
									->execute()
									->fetchAllAssoc();
		foreach ($arrNewsletterChannels as $arrNewsletterChannel) {
			if (intval($arrNewsletterChannel['notification_active']) === 1 && intval($arrNewsletterChannel['notification_on_unsubscribe']) === 1) {
				$email = new \Email();
				$email->subject = 'Abmeldung vom Newsletter "'.$arrNewsletterChannel['title'].'"';
				
				$strText = str_replace('##channel##', $arrNewsletterChannel['title'], $arrNewsletterChannel['notification_on_unsubscribe_text']);
				$strText = str_replace('##email##', $strEmail, $strText);
				$strText = str_replace('##domain##', \Idna::decode(\Environment::get('host')), $strText);
				
				$email->text = $strText;
				$email->sendTo($arrNewsletterChannel['notification_recipients']);
			}
		}
	}
}
