-- **********************************************************
-- *                                                        *
-- * IMPORTANT NOTE                                         *
-- *                                                        *
-- * Do not import this file manually but use the TYPOlight *
-- * install tool to create and maintain database tables!   *
-- *                                                        *
-- **********************************************************


-- 
-- Table `tl_newsletter_channel`
-- 
CREATE TABLE `tl_newsletter_channel` (
  `notification_active` char(1) NOT NULL default '0',
  `notification_on_subscribe` char(1) NOT NULL default '1',
  `notification_on_subscribe_text` mediumtext NULL,
  `notification_on_unsubscribe` char(1) NOT NULL default '1',
  `notification_on_unsubscribe_text` mediumtext NULL,
  `notification_recipients` mediumtext NULL,
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
