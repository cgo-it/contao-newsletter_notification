<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package newsletter_notification
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the namespaces
 */
ClassLoader::addNamespaces(array
(
	'cgoIT',
));


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	// Classes
	'cgoIT\newsletter_notification\NewsletterNotification' => 'system/modules/newsletter_notification/classes/NewsletterNotification.php',
));
