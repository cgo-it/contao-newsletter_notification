<?php

/**
 * Contao Open Source CMS
 * Copyright (C) 2005-2011 Leo Feyer
 *
 * Formerly known as TYPOlight Open Source CMS.
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this program. If not, please visit the Free
 * Software Foundation website at <http://www.gnu.org/licenses/>.
 *
 * PHP version 5
 * @copyright  cgo IT, 2014
 * @author     Carsten Götzinger (info@cgo-it.de)
 * @package    newsletter_notification
 * @license    GNU/LGPL
 * @filesource
 */

/**
 * palettes
 */
$GLOBALS['TL_DCA']['tl_newsletter_channel']['palettes']['__selector__'][] = 'notification_active';

$GLOBALS['TL_DCA']['tl_newsletter_channel']['palettes']['default'] .= ';{nl_notification_legend},notification_active';

/**
 * Add subpalettes to tl_newsletter_channel
 */
$GLOBALS['TL_DCA']['tl_newsletter_channel']['subpalettes']['notification_active']  = 'notification_on_subscribe,notification_on_subscribe_text,notification_on_unsubscribe,notification_on_unsubscribe_text,notification_recipients';

/**
 * fields
 */
$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_active'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_active'],
		'default'                 => '0',
		'exclude'                 => true,
		'inputType'               => 'checkbox',
		'eval'                    => array('tl_class'=>'w50 m12', 'submitOnChange'=>true)
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_on_subscribe'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_subscribe'],
		'exclude'                 => true,
		'inputType'               => 'checkbox',
		'eval'                    => array('tl_class'=>'w50 clr m12')
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_on_subscribe_text'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_subscribe_text'],
		'exclude'                 => true,
		'inputType'               => 'textarea',
		'eval'                    => array('style'=>'height:120px', 'decodeEntities'=>true, 'alwaysSave'=>true, 'tl_class'=>'clr long'),
		'load_callback' => array
		(
				array('tl_newsletter_channel_notification', 'getSubscribeDefault')
		)
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_on_unsubscribe'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_unsubscribe'],
		'exclude'                 => true,
		'inputType'               => 'checkbox',
		'eval'                    => array('tl_class'=>'w50 m12 clr')
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_on_unsubscribe_text'] = array
(
		'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_on_unsubscribe_text'],
		'exclude'                 => true,
		'inputType'               => 'textarea',
		'eval'                    => array('style'=>'height:120px', 'decodeEntities'=>true, 'alwaysSave'=>true, 'tl_class'=>'clr long'),
		'load_callback' => array
		(
				array('tl_newsletter_channel_notification', 'getUnsubscribeDefault')
		)
);

$GLOBALS['TL_DCA']['tl_newsletter_channel']['fields']['notification_recipients'] = array
(
	'label'                   => &$GLOBALS['TL_LANG']['tl_newsletter_channel']['notification_recipients'],
	'exclude'                 => true,
	'inputType'               => 'text',
	'eval'                    => array('tl_class'=>'long clr')
);

/**
 * Class tl_newsletter_channel_notification
 */
class tl_newsletter_channel_notification extends \Backend {

	/**
	 * Load the default subscribe text
	 * @param mixed
	 * @return mixed
	 */
	public function getSubscribeDefault($varValue)
	{
		if (!trim($varValue))
		{
			$varValue = $GLOBALS['TL_LANG']['tl_newsletter_channel']['text_subscribe'];
		}

		return $varValue;
	}


	/**
	 * Load the default unsubscribe text
	 * @param mixed
	 * @return mixed
	 */
	public function getUnsubscribeDefault($varValue)
	{
		if (!trim($varValue))
		{
			$varValue = $GLOBALS['TL_LANG']['tl_newsletter_channel']['text_unsubscribe'];
		}

		return $varValue;
	}
}
